import pandas as pd
import plotly
import plotly.express as px
import plotly.graph_objects as go
from dash import Dash, dcc, html, Input, Output
import numpy as np
import geojson
import geopandas as gpd
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import base64
import plotly.io as pio
from urllib.request import urlopen
import json


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = Dash(__name__, external_stylesheets = external_stylesheets)

df = pd.read_csv("final_data/All_Data.csv")
df = df.replace("-", 0.0, regex = True)
df["NumValue"] = df["NumValue"].astype(float).astype(int)
df = df.replace(0, np.NaN, regex = True)
df["Tested"] = df["Tested"].replace("-", 0, regex = True)
df.reset_index(inplace = True)
pd.to_numeric(df["NumValue"])
df = df.groupby(["RegionName", "Time", "NumValue"], as_index=False).first()
europe_geo = gpd.read_file("final_data/geo_data/europe.json")
# europe_geo2 = gpd.read_file("../geoData/country_shapes.geojson")
# europe_geo_3 = gpd.read_file("../geoData/europe.json.txt")



with urlopen('https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json') as response:
    counties = json.load(response)


description_options = {
    "Escherichia Coli" : ["Escherichia coli, also known as E. coli, is a Gram-negative coliform bacterium of the genus Escherichia that is commonly found in the lower intestine of warm-blooded organisms. Most E. coli strains are harmless, but some serotypes can cause serious food poisoning in their hosts, and are occasionally responsible for food contamination incidents that prompt product recalls.  Escherichia coli has the interesting characteristic of being both a widespread gut commensal bacteria in vertebrates and a versatile pathogen, resulting in killing more than 2 million humans per year through both intra- and extraintestinal diseases. Selective pressure in the habitats of commensal strains may coincidentally promote the emergence of antimicrobial resistance (AMR), rendering commensal E. coli strain reservoirs of AMR determinants. Emergence of antimicrobial resistance is one of the biggest global threat to public health. In addition to the use of antimicrobials in humans, the use of antimicrobials in livestock is considered an important driver of AMR, subsequently compromising human health."],
    "Acinetobacter spp" : ["Acinetobacter is a genus of gram-negative bacteria belonging to the wider class of Gammaproteobacteria. Acinetobacter species are oxidase-negative, exhibit twitching motility, and occur in pairs under magnification. The most striking characteristic of Acinetobacter spp. is their natural resistance to many antibiotics and the ability to easily develop new resistances under antibiotic pressure. They overexpress efflux pumps, harbor β-lactamases, and are characterized by low membrane permeability. In 2017, carbapenem-resistant Acinetobacter caused an estimated 8,500 infections in hospitalized patients and 700 estimated deaths in the United States. Many Acinetobacter germs are resistant to many antibiotics, including carbapenems, which makes them difficult to treat with available antibiotics. "],
    "Klebsiella pneumoniae" : ["Klebsiella pneumoniae is a gram-negative, encapsulated, non-motile bacterium found in the environment and has been associated with pneumonia in patient populations with alcohol use disorder or diabetes mellitus. The bacterium typically colonizes human mucosal surfaces of the oropharynx and gastrointestinal tract. Once the bacterium enters the body, it can display high degrees of virulence and antibiotic resistance. Today, K. pneumoniae pneumonia is considered the most common cause of hospital-acquired pneumonia in the United States, and the organism accounts for 3% to 8% of all nosocomial bacterial infections. Multi-drug resistant Klebsiella pneumoniae is a major nosocomial pathogen, causing infections with high morbidity and mortality rates (up to 50%). As many as 90,000 infections and more than 7,000 deaths in Europe are attributable to K. pneumoniae resistant to carbapenems"]


}

all_options = {
    "Escherichia Coli" : ["Fluoroquinolones","Cephalosporins","Aminophenicillins","Aminoglycosides"],
    "Acinetobacter spp" : ["Combined Resistance","Fluoroquinolones","Carbapanems","Aminoglycosides"],
    "Klebsiella pneumoniae" : ["Combined Resistance","Fluoroquinolones","Aminoglycosides"]
}

picture_option = {
    "Escherichia Coli" : ["assets/ecoli.png"],
    "Acinetobacter spp" : ["assets/acenitobacter.png"],
    "Klebsiella pneumoniae" : ["assets/klebsiella.png"],
}




what_is = "Antimicrobial Resistance (AMR) occurs when bacteria, viruses, fungi and parasites change over time and no longer respond to medicines making infections harder to treat and increasing the risk of disease spread, severe illness and death. As a result of drug resistance, antibiotics and other antimicrobial medicines become ineffective and infections become increasingly difficult or impossible to treat. Learn more by watching WHO's short video!"
why_is = "The emergence and spread of drug-resistant pathogens that have acquired new resistance mechanisms, leading to antimicrobial resistance, continues to threaten our ability to treat common infections. Especially alarming is the rapid global spread of multi-resistant bacteria (also known as “superbugs”) that cause infections that are not treatable with existing antimicrobial medicines such as antibiotics. Antibiotics are becoming increasingly ineffective as drug-resistance spreads globally leading to more difficult to treat infections and death. New antibacterials are urgently needed – for example, to treat carbapenem-resistant gram-negative bacterial infections as identified in the WHO priority pathogen list. However, if people do not change the way antibiotics are used now, these new antibiotics will suffer the same fate as the current ones and become ineffective."
how_to = "This interactive figure shows the development of resistance in 3 common bacteria within Europe. The color shows how severe the resistance to the chosen antibiotic is currently within the selected bacterium.  The size of the bubbles above the regions shows how many pathogens have been tested to determine the percentage of resistance. Use the time slider to see the development over the years."

app.layout = html.Div(children=[

    html.H1("Antimicrobial Resistance Map", style={'text-align': 'center','background-image': 'url("/assets/background_blue.png")'}),
    html.Div(style ={'width':'100%', 'display':'inline-block'},children = [
        html.Div(style = {"float":"left","width":"70%"}, children = [
            html.H3("What is Antimicrobial Resistance (AMR) ?",style={'text-align': 'center','width': '60%'}),
            html.H5(what_is, style={'background-image': 'url("/assets/background.png")','text-align': 'center','width': '60%'}),
            html.Br(),
            html.H3("Why is antimicrobial resistance a global concern?", style = {'text-align': 'center','width': '60%'}),
            html.H5(why_is, style={'background-image': 'url("/assets/background.png")','text-align': 'center','width': '60%'}),]),

        html.Div([
            # html.P("If you want to lern more about AMR, watch this video!", style={'text-align': 'center'}),
            html.Div(style = {'width':'30%', 'float':'right'}, children = [
            html.Br(),
            html.Br(),
            html.Br(),
            html.Video(style = {'background-image': 'url("/assets/background.png")',"height":"100%",'width':'50%', 'display':'inline-block', 'overflow': 'hidden'},controls = True, src = "/assets/who_video.mp4", autoPlay=False),
            html.Div([
                html.H3("How to use this map?",style={'text-align': 'center'}),
                html.H5(how_to, style={'background-image': 'url("/assets/background.png")','text-align': 'center'}),

            ]),
            ]),
        ]),

    ]),

    html.Br(),

    html.Label(children = ["Select Year",
        dcc.Slider(
            id='time',
        )]),

    html.Br(),

    html.Div(style={'width': '100%','display':'inline-block','overflow': 'hidden'},children = [
        html.H3("Information about the selected organism:"),

        html.Div(id = "description", children=["init"],style={'text-align': 'center','fontSize': 25,'background-image': 'url("/assets/background.png")','width': '30%','display':'inline-block','overflow': 'hidden','float':'left'}),
        html.Div(style={'width': '20%','display':'inline-block','overflow': 'hidden','float':'right'},children = [
                html.Br(),
            html.Label(style={'width': '70%','display':'inline-block'}, children = ["Select Organism",
                dcc.Dropdown(
                    id="Organism",
                    options=[{"label":k, "value":k} for k in all_options.keys()],
                    # multi=False,
                    value="Escherichia Coli",
                    style={'width': "100%"}),
                    ]),
                    html.Br(),
                    html.Br(),

            html.Label(style={'width': '70%','display':'block'}, children = ["Select Antibiotic Agent",
                dcc.Dropdown(id='Antibiotic'),
                html.Br(),
                html.Hr( style={'text-align': 'center','display': 'inline-block'}),
                html.Br(),]),
            html.P("Here is a picture of the selected organism:"),
            html.Img(style = {"width":"400px"}, id = "bild"),
            html.Br(),
            html.Br(),
            html.Br(),
            html.Br(),
            html.P("This data comes from the European Center for Disease Prevention and Control and is accessible via this link: "),
            html.A("Click Here", href = "https://www.ecdc.europa.eu/en/publications-data"),
        ]),
        html.Br(),
        dcc.Graph(id='map', figure={}, style={'width': '50%','display':'inline-block','float':'right'}),
         ]),
         # html.Img(style={'width': '20%' ,'vertical-align': 'bottom','float':'left'}, src='data:image/png;base64,{}'.format(pct_base64)),

        html.H3("What can be done against AMR?"),
        html.Div(style = {'background-image': 'url("/assets/background.png")'}, children = [
        html.Div(style = {'width':'50%','background-image': 'url("/assets/background.png")', 'display':'inline-block','overflow': 'hidden','float':'left'}, children = [
            html.Div("1) Do not use antibiotics to treat viral infections, such as influenza, the common cold, a runny nose or a sore throat. Ask your doctor for other ways to feel better.",style={'fontSize': 25}),
            html.Div("2) Improve sanitation and prevent the spread of infection ",style={'fontSize': 25}),
            html.Div("3) Promote new and rapid diagnostics",style={'fontSize': 25}),
            html.Div("4) Promote development and use of alternatives",style={'fontSize': 25}),
            html.Div("5) Better incentives to promote investment for new drugs and improving existing ones",style={'fontSize': 25}),
        ]),
        html.Div(style = {'width':'50%','background-image': 'url("/assets/background.png")', 'display':'inline-block','overflow': 'hidden','float':'right'}, children = [
            html.Div("6) Generate global public awareness", style={'fontSize': 25}),
            html.Div("7) When you are prescribed antibiotics, take the full prescription even if you are feeling better. Ensure that members of your family do the same.",style={'fontSize': 25}),
            html.Div("8) Never share antibiotics with others or use leftover prescriptions.",style={'fontSize': 25}),
            html.Div("9) Remember, each time you take an antibiotic when it is not necessary, the effectiveness of the antibiotic decreases and it might not work the next time you really need it.",style={'fontSize': 25}),
        ]),
        ]),


])

@app.callback(
    Output('Antibiotic', 'options'),
    Input('Organism', 'value'))


def set_antibiotic_options(selected_organism):
    return [{'label': i, 'value': i} for i in all_options[selected_organism]]

@app.callback(
    Output('description', 'children'),
    Input('Organism', 'value'))


def set_description(selected_organism):
    return description_options[selected_organism]

@app.callback(
    Output('bild', 'src'),
    Input('Organism', 'value'))


# pct = 'pictures/ecoli.png'
#
# pct_base64 = base64.b64encode(open(pct, 'rb').read()).decode('ascii')


def set_image(selected_organism):
    pct =  str(picture_option[selected_organism][0])
    print(pct)
    pct_base64 = base64.b64encode(open(pct, 'rb').read()).decode('ascii')
    src = 'data:image/png;base64,{}'.format(pct_base64)
    return src


@app.callback(
    Output('Antibiotic', 'value'),
    Input('Antibiotic', 'options'))

def set_antibiotic_value(available_options):
    return available_options[0]['value']

@app.callback(
    [Output('time', 'min'),
    Output('time','max'),
    Output('time', 'step'),
    Output('time','marks'),],
    [Input('Organism', 'value'),
    Input('Antibiotic', 'value')]
)

def set_min_max_time(Organism, Antibiotic):
    df_time = df
    df_time = df_time[df_time["Organism"] == Organism]
    df_time = df_time[df_time["Antibiotic"] == Antibiotic]

    min = df_time['Time'].min(),
    max = df_time['Time'].max(),
    marks = range(min[0], max[0]+1)
    marks = list(marks)
    marks = {str(elem) : str(elem) for elem in marks}
    step = 1
    print(min, max)
    return min[0], max[0], step, marks






@app.callback(
    Output(component_id='map', component_property='figure'),
    [Input(component_id='time', component_property='value'),
    Input(component_id='Antibiotic', component_property='value'),
    Input(component_id='Organism', component_property='value'),]
)



def update_graph(time,Antibiotic,Organism):
    # print(option_slctd)
    # print(type(option_slctd))

    container_time = "Choosen Year: {}".format(time)
    container_antibiotics = "Choosen Antibiotic: {}".format(Antibiotic)
    container_organism = "Choosen Organism: {}".format(Organism)
    print(container_time, container_organism, container_antibiotics)

    dff = df
    dff = dff[dff["Organism"] == Organism]
    dff = dff[dff["Antibiotic"] == Antibiotic]
    dff = dff[dff["Time"] == time]
    tested = dff["Tested"].tolist()
    normalized_tested = [((int(i)-min(tested))/(max(tested)-min(tested)))*100 for i in tested]


    fig = px.choropleth(
        data_frame=dff,
        title = "Antimicrobial Resistance Map",
        geojson = europe_geo,
        locationmode='country names',
        locations="RegionName",
        scope="europe",
        color='NumValue',
        hover_data=['RegionName','NumValue',"Tested"],
        color_continuous_scale=px.colors.sequential.YlOrRd,
        range_color = (0,100),
        labels={'NumValue': '% of resistant bacteria'},
        animation_frame="Time",
        template = "seaborn",

    )
    # fig.update_layout(coloraxis_colorbar_y=-0.01)

    fig.update_layout(
        autosize=True,
        margin={'r':5,'t':0,'l':0,'b':0},
        width=835,
        height=900,
        # margin=dict(
        #     l=50,
        #     r=50,
        #     b=100,
        #     t=100,
        #     pad=4
        # ),
        # paper_bgcolor="LightSteelBlue",
    )
    fig.update_geos(projection_scale = 1.65)

    fig.add_trace(go.Scattergeo(
        # data_frame=dff,
        geojson = counties,
        showlegend = True,
        # mode='markers',
        # mode='text',
        hoverinfo = 'text',
        text = dff['Tested'],
        name = "Amount of tested samples",
        locationmode = 'country names',
        locations=dff["RegionName"],
        marker = dict(
                size = normalized_tested,
                autocolorscale = True,
                color = "#8A2BE2",
                # colorbar=True,

        )

    )
    )

    fig.update_layout(
    legend=dict(
        x=0,
        y=1,
        traceorder="reversed",
        title_font_family="Arial",
        font=dict(
            family="Courier",
            size=20,
            color="black"
        ),
        # bgcolor="LightSteelBlue",
        # bordercolor="Black",
        # borderwidth=2
    ))


    return fig

if __name__ == '__main__':
    app.run_server(host = '127.0.0.1', debug=True)
