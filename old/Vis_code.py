import pandas as pd
import plotly
import plotly.express as px
import plotly.graph_objects as go
from dash import Dash, dcc, html, Input, Output
import numpy as np
import geojson
import geopandas as gpd
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = Dash(__name__)

df = pd.read_csv("/mnt/c/Users/Mustafa/Desktop/Uni/Master/DataVis/data-visualization-project/final_data/All_Data.csv")
df = df.replace("-", 0.0, regex = True)
df["NumValue"] = df["NumValue"].astype(float).astype(int)
df = df.replace(0, np.NaN, regex = True)
df.reset_index(inplace = True)
pd.to_numeric(df["NumValue"])
df = df.groupby(["RegionName", "Time", "NumValue"], as_index=False).first()
europe_geo = gpd.read_file("final_data/geo_data/europe.json")


lon = []
lat = []

for i in range(len(europe_geo)):
 if(Polygon.contains(Point(europe_geo["Longitude"][i],europe_geo["Latitude"][i]))):
  lon.append(europe_geo["Longitude"][i])
  lat.append(europe_geo["Longitude"][i])
  # year.append(europe_geo[‘Year’][i])
  # mob.append(europe_geo[‘Year’][i])

print(lon)

all_options = {
    "Escherichia Coli" : ["Fluoroquinolones","Cephalosporins","Aminophenicillins","Aminoglycosides"],
    "Acinetobacter spp" : ["Combined Resistance","Fluoroquinolones","Carbapanems","Aminoglycosides"],
    "Klebsiella pneumoniae" : ["Combined Resistance","Fluoroquinolones","Aminoglycosides"]
}


app.layout = html.Div([

    html.H1("Bacterial Resistance Map", style={'text-align': 'center'}),

    dcc.Dropdown(
        id="Organism",
        options=[{"label":k, "value":k} for k in all_options.keys()],
        # multi=False,
        value="Escherichia Coli",
        style={'width': "100%"}
    ),

    html.Hr(),

    dcc.Dropdown(id='Antibiotic'),

    html.Hr(),

    dcc.Slider(
        id='time',
        min=df['Time'].min(),
        max=df['Time'].max(),
        value=df['Time'].max(),
        marks={str(year): str(year) for year in df['Time'].unique()},
        step=None
    ),

    html.Div(id='output_container', children=[]),
    html.Br(),
    dcc.Graph(id='map', figure={}, style={'width': "100%",'height':"100%"})

])

@app.callback(
    Output('Antibiotic', 'options'),
    Input('Organism', 'value'))

def set_antibiotic_options(selected_organism):
    return [{'label': i, 'value': i} for i in all_options[selected_organism]]

@app.callback(
    Output('Antibiotic', 'value'),
    Input('Antibiotic', 'options'))

def set_antibiotic_value(available_options):
    return available_options[0]['value']


@app.callback(
    [Output(component_id='output_container', component_property='children'),
     Output(component_id='map', component_property='figure')],
    [Input(component_id='time', component_property='value'),
    Input(component_id='Antibiotic', component_property='value'),
    Input(component_id='Organism', component_property='value')]
)
def update_graph(time,Antibiotic,Organism):
    # print(option_slctd)
    # print(type(option_slctd))

    container_time = "Choosen Year: {}".format(time)
    container_antibiotics = "Choosen Antibiotic: {}".format(Antibiotic)
    container_organism = "Choosen Organism: {}".format(Organism)
    print(container_time, container_organism, container_antibiotics)
    dff = df
    dff = dff[dff["Organism"] == Organism]
    dff = dff[dff["Antibiotic"] == Antibiotic]
    dff = dff[dff["Time"] == time]

    fig = px.choropleth(
        data_frame=dff,
        geojson = europe_geo,
        locationmode='country names',
        locations="RegionName",
        scope="europe",
        color='NumValue',
        hover_data=['RegionName','NumValue',"Tested"],
        color_continuous_scale=px.colors.sequential.YlOrRd,
        range_color = (0,100),
        width=1500,
        height = 800,
        labels={'Percent of Resistance': '% of Bacterials'},
        animation_frame="Time",
    )

    fig.add_trace(go.Scattergeo(
        mode='markers',
        # geojson = europe_geo,
        lon = [9.00000],
        lat = [53.0000]
    ))


    return container_time, fig

if __name__ == '__main__':
    app.run_server(debug=True)






###############      Graveyard            #########################################

    # dcc.Dropdown(id="Organism",
    #     options=[
    #         {"label": "Acinetobacter spp", "value": "Acinetobacter spp"},
    #         {"label": "Escherichia Coli", "value": "Escherichia Coli"},
    #         {"label": "Klebsiella pneumoniae", "value": "Klebsiella pneumoniae"}],
    #     multi=False,
    #     value="Escherichia Coli",
    #     style={'width': "100%"}
    # ),

    # dcc.Dropdown(id="Antibiotic",
    #     options=[
    #         {"label": "Aminoglycosides", "value": "Aminoglycosides"},
    #         {"label": "Carbapanems", "value": "Carbapanems"},
    #         {"label": "Fluoroquinolones", "value": "Fluoroquinolones"},
    #         {"label": "Combined_Resistance", "value": "Combined_Resistance"},
    #         {"label": "Cephalosporins", "value": "Cephalosporins"},
    #         {"label": "Aminophenicillins", "value": "Aminophenicillins"}],
    #     multi=False,
    #     # value="Combined_Resistance",
    #     style={'width': "100%"}
    # ),
    # dcc.Dropdown(id="time",
    #      options=[
    #          {"label": "2010", "value": 2010},
    #          {"label": "2011", "value": 2011},
    #          {"label": "2012", "value": 2012},
    #          {"label": "2013", "value": 2013},
    #          {"label": "2014", "value": 2014},
    #          {"label": "2015", "value": 2015},
    #          {"label": "2016", "value": 2016},
    #          {"label": "2017", "value": 2017},
    #          {"label": "2018", "value": 2018},
    #          {"label": "2019", "value": 2019},
    #          {"label": "2020", "value": 2020}],
    #      multi=False,
    #      value=2015,
    #      style={'width': "100%"}
    #  ),
