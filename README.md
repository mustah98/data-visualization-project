# Antimicrobial Resistance Map For Europe

<p align="center">
<img src="pictures/vis.PNG" width="700">
</p>

# How to execute the code

1.  download anaconda (https://docs.anaconda.com/anaconda/install/index.html)
2.  build the environment using the datavis.yml file
```
conda env create -f datavis.yml
```
3.  activate environment using conda
```
conda activate datavis
```
4.  run the code using python
```
python3 Vis_code.py
```
5.  enter your browser on <http://127.0.0.1:8050/>


# About this project

This project was created as part of the master course Data Visualizations. All external content in this directory is cited. Everything not cited was created by myself, Mustafa Helal. 

# Introduction

Antimicrobial Resistance (AMR) occurs when bacteria, viruses, fungi and parasites change over time and no longer respond to medicines making infections harder to treat and increasing the risk of disease spread, severe illness and death. 
As a result of drug resistance, antibiotics and other antimicrobial medicines become ineffective and infections become increasingly difficult or impossible to treat.
The emergence and spread of drug-resistant pathogens that have acquired new resistance mechanisms, leading to antimicrobial resistance, continues to threaten our ability to treat common infections. Especially alarming is the rapid global spread of multi- and pan-resistant bacteria (also known as “superbugs”) that cause infections that are not treatable with existing antimicrobial medicines such as antibiotics.

The aim of this visualization is to present a rather complex problem of human and veterinary medicine in a simple and understandable way.  On the one hand, it serves to make the general population aware of the problems emanating from AMR and, on the other hand, to encourage political committees in the health sector to create action plans for a more optimized and efficient use of antibiotics.

In addition, pharmaceutical companies should be encouraged to develop new antibiotics to replace old ones that are becoming increasingly ineffective.  At present, pharmaceutical companies are opposed to the development of new antibiotics, as this is often not profitable. However, the spread of resistance is not subject to greed, so companies should be obliged to contribute a certain part to the preservation of public health.

## Context of this visualization

This visualization serves to illustrate current AMR (Antimicrobial Resistance) developments in current pathogens across Europe. AMR occurs when bacteria, viruses, fungi and parasites change over time and no longer respond to medicines making infections harder to treat and increasing the risk of disease spread, severe illness and death.  According to the WHO (World Health Organisation), the emergence, persistance and transmission of resiststant phatogens constitute a major thread to medical practice and public health. Because of this, it is important to provide opportunities to visually bring people closer to the problem through AMR, specially to health authorities.


## Origin of the data set

The data were collected by the ECDC ( European Centre for Disease Prevention and Control) [https://www.ecdc.europa.eu/en/about-us/ecdcs-mission-and-main-activities] , and made available to the public. ECDC is an agency aimed at strengthening Europe's defences against infectious diseases, therefor this project is funded by the European Commission.  The data is collected via an European Antimicrobial Resistance Surveillance Network (EARS-Net) with guidelines for reporting countries. The data is collected and aggregated on an annual basis. It consists of confirmed case numbers in tested pathogens.

## Domain Characterization 

To get an overview of the target group, I asked wihtin my social environment what people associate with the term antimicrobial resistance.  From the survey, it was evident that only a smaller proportion could associate something with the term antimicrobial resistance. Accordingly, the following hurdles had to be mastered first.

-   Explain the concept of antimicrobial resistance.
-   Explain the emergence of resistance in simple and not highly scientific ways.
-   And to make the scale of the problem more understandable, explain what problems arise from widespread resistance and why this problem is a problem of the general public rather than the individual. 

Thus, an easy-to-understand visualization was needed that explained the background of the problem in a comprehensible way and made people feel addressed. A second survey was conducted with individuals in the science field. In this environment, the term antimicrobial resistance is thoroughly familiar, as are the problems that arise from it. However, the extent of the problem was estimated to be far less than could be determined from the data. Accordingly, this has resulted in the following visualization design challenge. 

-   The scope of the problem must be made clear at a higher level

Accordingly, it is necessary to visualize the scope of the problem by presenting concrete values, taking the individuality out of it by looking at the whole of Europe, the evolution into the "bad" by introducing a timeline that shows the gradual spread of the problem and the associated risks to health. 

## Data Abstraction

The first step behind any good visualization is data preparation. To do this, it was important to determine which data points were meaningful enough to convey a message. And for this it was also important to confirm the completeness of the data. The dataset is from the European Centre for Disease Prevention and Control. For this purpose, a project for antibiotic resistance surveillance was established, in which European countries participated. The data from this large-scale project are publicly available and usable and are yearly updated. The project is supported and financed by the European Commission. In this context, the most important indicator, the percentage of resistant isolates, is determined by performing drug susceptibility tests (DST). These are laboratory procedures to detect resistance or susceptibility of isolates.  HHowever, the DST approach was not listed in the description of the dataset. The dataset that was finaly used for this visualization, looked like this: 
<!-- TABLE_GENERATE_START -->
|Organism|Antimicrobial Agent|Indicators  [Resistence / Percentage, Total tested isolates, Susceptical isolates]|Regions|Time|
| -------------|--------------|----------------------------------------------------------------------------------|-------|----|
|Klebsiella pneumoniae|Carbapenems|[%                      ,   Number              , Number]|Germany|2010 (in years)|
|...|...|...|...|...|
<!-- TABLE_GENERATE_END -->

-   Organism: This column lists phatogens that have been studied in the project. These are mainly pathogens that are more affected by the problem of AMR. 
-   Antimicrobial Agent: This column lists drugs that have been tested for possible inefficacy. Inefficacy refers to the development of resistance within the pathogens to one or more of these drugs. 
-   Indicators: Here the data is now aggregated. In addition to the two previous data points, this column now contains the determined resistance in percent, the number of susceptical isolates and the number of tested isolates.
-   Region: All the Previous data points were collected in various countries within Europe that were participating in the project. 
-   Time: All of the Previous data points were collected over several years (insegsamt from 2000 - 2020, but the timings vary within certain combinations).

## Task Abstraction

The task of this visualization can be well summarized in 3 parts: 

1.) The data is used for information about the percentage of resistant germs per country.
2.) The data is used for information about the number of isolates tested per country.
3.) The data is used for information about the development of resistance over time per organism and per antibiotic.

## Visual Encoding

The dashboard consists of one main visualization, a choropleth map combined with a bubble annotations. The map consists of different yellow-red gradient colors. Each country is color-annotated and the color indicates the percentage of resistant isolates identified in that country. The closer the color is to red (a color that emits danger), the more severe the extent of resistance in this country. In addition, each country is assigned a bubble. The size of the bubble indicates how many isolates the country has tested for resistance in the respective year.  Unfortunately there is no legend for this, as the package used (Plolty) does not yet support this and rely on donations to implement this (see Plotly GitHub Open Issues). The color of the bubbles was chosen to be blue, so that they can be distinguished from the annotated countries. One possibility would be to use a cartogram to combine both attributes, but this visualization was not visually appealing due to the high varriation of tested samples within the countries and resulted in incomprehensibility. Nevertheless, the focus was set on creating an all-encompassing visualization in order to make it as comfortable as possible for the user, who does not have to search for individual pieces of information between several visualizations in order to understand the context of these. The visualization supports data from several years. Accordingly, a timeline exists above the visualization, which can be used to interactively see dynamics and recognize trends, or simply to compare different years with each other. 



## Interaction Design

The visualization supports hovering to display the data associated with the colors and bubbles. In addition, you can zoom into the visualization, for example, to take a closer look at geographically smaller countries. Above the visualization there is a tool row that can be used to selectively choose countries that one would like to view individually. In addition, a screenshot can be created via one click, which is then saved.   

## Reflection

Unfortunately, it was not possible to host this project via free providers, as the application requires more than freely accessible memory. Moreover, I should have realized beforehand that no bubble-size legend is supported by Plotly. Sadly, I realized this too late in the course of the project, so rough changes to libraries were no longer possible. Also, there are some layout issues within the Dash board, making it necessary to resize the browser individually to see everything. Since this was my very first approach to create such an html based visualization (dash board), I consider the final result quite a success. 

## Screencast

See screencast is located at this link: https://www.youtube.com/watch?v=60xoiskGXKQ 

## Presentation
The final presentation in acc via this link:  https://drive.google.com/file/d/1dCwshT-u8piiyHYMY0UdI_KQpSaDblUL/view?usp=sharing 

# Impressum

## Legal Notice according to § 5 TMG (Telemediengesetz)

Claudia Müller-Birn
Forschungsgruppe Human-Centered Computing
Freie Universität Berlin
Königin-Luise-Str. 24/26
14195 Berlin

Contact: clmb@inf.fu-berlin.de

## Privacy Statement
We do not collect any data on this website.

##Liability for Links
Our site contains links to third-party Web sites. We have no influence whatsoever on the information on these Web sites and accept no guaranty for its correctness. The content of such third-party sites is the responsibility of the respective owners and or providers. At the time third-party Web sites were linked to ours, we found NO GROUNDS WHATSOEVER of any likely contravention of the law. We shall promptly delete a link upon becoming aware that it violates the law.

## Attribution

AMR: https://www.who.int/health-topics/antimicrobial-resistance

Picture E.Coli: https://www.nationalgeographic.org/encyclopedia/escherichia-coli-e-coli/
Picture Klebsiella: https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Klebsiella-pneumoniae.jpg/300px-Klebsiella-pneumoniae.jpg
Picure Acetinobacter: https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Acinetobacter_baumannii.JPG/300px-Acinetobacter_baumannii.JPG 






